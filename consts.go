/*
Copyright [2017] [morran]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sx1278

import rpio "github.com/stianeikeland/go-rpio"

const (
	// DefaultTimeout default value for timeout while transmitting/receiving data from transiever
	DefaultTimeout = 30000
)

// Registers of LoRa transiever
const (
	maxPacketLength        byte = 127
	regDIOMapping1         byte = 0x40
	regDIOMapping2         byte = 0x41
	regVersion             byte = 0x42
	regTcxo                byte = 0x4B
	regPaDac               byte = 0x4D
	regFormerTemp          byte = 0x5B
	regAgcRef              byte = 0x61
	regAgcThresh1          byte = 0x62
	regAgcThresh2          byte = 0x63
	regAgcThresh3          byte = 0x64
	regFifo                byte = 0x00
	regOpMode              byte = 0x01
	regFrMsb               byte = 0x06
	regFrMid               byte = 0x07
	regFrLsb               byte = 0x08
	regPaConfig            byte = 0x09
	regPaRamp              byte = 0x0a
	regOcp                 byte = 0x0b
	regLna                 byte = 0x0c
	regFifoAddrPtr         byte = 0x0d
	regFifoTxBaseAddr      byte = 0x0e
	regFifoRxBaseAddr      byte = 0x0f
	regFifoRxCurrentAddr   byte = 0x10
	regIrqFlagsMask        byte = 0x11
	regIrqFlags            byte = 0x12
	regRxNbBytes           byte = 0x13
	regRxHeaderCntValueMsb byte = 0x14
	regRxHeaderCntValueLsb byte = 0x15
	regRxPacketCntValueMsb byte = 0x16
	regRxPacketCntValueLsb byte = 0x17
	regModemStat           byte = 0x18
	regPktSnrValue         byte = 0x19
	regPktRssiValue        byte = 0x1a
	regRssiValue           byte = 0x1b
	regHopChannel          byte = 0x1c
	regModemConfig1        byte = 0x1d
	regModemConfig2        byte = 0x1e
	regSymbTimeoutLsb      byte = 0x1f
	regPreambleMsb         byte = 0x20
	regPreambleLsb         byte = 0x21
	regPayloadLength       byte = 0x22
	regMaxPayloadLength    byte = 0x23
	regHopPeriod           byte = 0x24
	regFifoByteAddr        byte = 0x25
	regModemConfig3        byte = 0x26
	regFeiMsb              byte = 0x28
	regFeiMid              byte = 0x29
	regFeiLsb              byte = 0x2a
	regRssiWideband        byte = 0x2c
	regDetectOptimize      byte = 0x31
	regInvertIQ            byte = 0x33
	regDetectionThreshold  byte = 0x37
	regSyncWord            byte = 0x39
)

// Available frequencies
const (
	Frequency432MHz = iota
	Frequency433MHz
	Frequency434MHz
	Frequency435MHz
)

// Available power configuration
const (
	Power17DBm = iota
	Power14DBm
	Power12_3DBm
	Power11DBm
	Power8DBm
	Power7_5DBm
	Power7DBm
	Power6DBm
	Power5DBm
	Power4DBm
)

// Spreading factor
const (
	SpreadingFactor6 byte = iota + 6
	SpreadingFactor7
	SpreadingFactor8
	SpreadingFactor9
	SpreadingFactor10
	SpreadingFactor11
	SpreadingFactor12
)

// Coding rate
const (
	CodingRrate4to5 byte = iota
	CodingRrate4to6
	CodingRrate4to7
	CodingRrate4to8
)

// Band width
const (
	BandWidth7_8KHz byte = iota
	BandWidth10_4KHz
	BandWidth15_6KHz
	BandWidth20_8KHz
	BandWidth31_2KHz
	BandWidth41_7KHz
	BandWidth62_5KHz
	BandWidth125KHz
	BandWidth250KHz
	BandWidth500KHz
)

// LNA Gain
const (
	G1 byte = iota + 1
	G2
	G3
	G4
	G5
	G6
)

// Modes
const (
	ModeFSK                      byte = 0x0
	ModeLORA                     byte = 0x80
	ModeLowFrequency             byte = 0x8
	ModeSleep                    byte = 0x0
	ModeStandby                  byte = 0x1
	ModeFrequencySyntesisTX      byte = 0x2
	ModeTransmit                 byte = 0x3
	ModeFrequencySyntesisRX      byte = 0x4
	ModeRXContinious             byte = 0x5
	ModeRXSingle                 byte = 0x6
	ModeChannelActivityDetection byte = 0x7
)

// IRQ Flags Masks
const (
	MaskRxTimeout         byte = 0x80
	MaskRxDone            byte = 0x40
	MaskPayloadCrcError   byte = 0x20
	MaskValidHeader       byte = 0x10
	MaskTxdone            byte = 0x8
	MaskCadDone           byte = 0x4
	MaskFhssChangeChannel byte = 0x2
	MaskCadDetected       byte = 0x1
)

// IRQ Flags
const (
	FlagRxTimeout         byte = 0x80
	FlagRxDone            byte = 0x40
	FlagPayloadCrcError   byte = 0x20
	FlagValidHeader       byte = 0x10
	FlagTxdone            byte = 0x8
	FlagCadDone           byte = 0x4
	FlagFhssChangeChannel byte = 0x2
	FlagCadDetected       byte = 0x1
)

// Modem status
const (
	MSModemClear        byte = 0x10
	MSHeaderInfoValit   byte = 0x8
	MSRXOnGoing         byte = 0x4
	MSSignalSyncronized byte = 0x2
	MSSignalDetected    byte = 0x1
)

var (
	currentMode byte = 0x4
)

var (
	// Power keeps part of available power configuration values
	Power = [...]byte{
		0xff, //17DBm
		0xfc, //14DBm
		0xfa, //12.3DBm
		0xF9, //11DBm
		0xF6, //8DBm
		0xf5, //7.5DBm
		0xf4, //7DBm
		0xf3, //6DBm
		0xf2, //5DBm
		0xf1, //4DBm
	}
	// Frequency keeps precalculated configuration of available frequencies
	Frequency = [...][]byte{
		{0x6c, 0x00, 0x00}, //432MHz
		{0x6c, 0x40, 0x00}, //433MHz
		{0x6C, 0x80, 0x00}, //434MHz
		{0x6C, 0xc0, 0x00}, //435MHz
	}
	// DIOs keep pairs od pins and their names
	DIOs    map[string]rpio.Pin
	pinNums = map[string]int{
		"DIO0":  17,
		"DIO1":  18,
		"DIO2":  21,
		"DIO3":  22,
		"DIO4":  23,
		"DIO5":  24,
		"RESET": 26,
	}
)

// Error ...
type Error struct {
	err string
}

func (e Error) Error() string {
	return e.err
}

// Errors
var (
	ErrorPayloadSize = Error{"Payload size must be less than 256"}
	ErrorTimeout     = Error{"Timeout"}
)
