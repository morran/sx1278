/*
Copyright [2017] [morran]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sx1278

func (d *Device) singleWrite(addr, data byte) error {
	return d.port.Tx([]byte{addr | 0x80, data}, nil)
}

func (d *Device) singleRead(addr byte) (byte, error) {
	recv := make([]byte, 2)
	err := d.port.Tx([]byte{addr, 0x00}, recv)
	if err != nil {
		return 0, err
	}
	return recv[1], nil
}

func (d *Device) burstRead(addr byte, size byte) ([]byte, error) {
	recv := make([]byte, size+1)
	dataToSend := []byte{addr}
	for i := byte(0); i <= size; i++ {
		dataToSend = append(dataToSend, 0x00)
	}
	err := d.port.Tx(dataToSend, recv)
	if err != nil {
		return []byte{}, err
	}
	return recv[1:], nil
}

func (d *Device) burstWrite(addr byte, data []byte) error {
	var dataToSend []byte
	dataToSend = append(dataToSend, addr|0x80)
	dataToSend = append(dataToSend, data...)
	return d.port.Tx(dataToSend, nil)
}
