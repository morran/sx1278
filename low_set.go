/*
Copyright [2017] [morran]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sx1278

import (
	"encoding/binary"
)

func (d *Device) setModemConfig1(bw, cr byte, implicitHMOn bool) error {
	return d.singleWrite(regModemConfig1, bw<<4|cr<<1|boolToByte(implicitHMOn))
}

func (d *Device) setModemConfig2(sf byte, modeTXC, crcON bool) error {
	return d.singleWrite(regModemConfig2, sf<<4|boolToByte(modeTXC)<<3|boolToByte(crcON)<<2)
}

func (d *Device) setModemConfig3(LDROOn, AAOn bool) error {
	return d.singleWrite(regModemConfig3, boolToByte(LDROOn)<<3|boolToByte(AAOn)<<2)
}

func (d *Device) setBaseMode(mode byte) error {
	var err error
	currentMode, err = d.singleRead(regOpMode)
	if err != nil {
		return err
	}
	currentMode = (currentMode & 0x7) | (mode & 0xf8)
	return d.singleWrite(regOpMode, currentMode)
}

func (d *Device) setMode(mode byte) error {
	return d.singleWrite(regOpMode, (currentMode&0xf8)|mode)
}

func (d *Device) clearIRQ() error {
	return d.singleWrite(regIrqFlags, 0xff)
}

func (d *Device) setPreambleLength(length uint16) error {
	buffer := make([]byte, 2)
	binary.LittleEndian.PutUint16(buffer, length)
	return d.burstWrite(regPreambleMsb, buffer)
}

func (d *Device) setPaConfig(PaSelect bool, MP, OP byte) error {
	return d.singleWrite(regPaConfig, boolToByte(PaSelect)<<7|MP<<4|OP)
}

func (d *Device) setFIFO(data []byte) error {
	if len(data) > 256 {
		return ErrorPayloadSize
	}
	FIFOPtrAddr := byte(0x80)
	if len(data) > 128 {
		FIFOPtrAddr = 0
	}
	err := d.singleWrite(regFifoAddrPtr, FIFOPtrAddr)
	if err != nil {
		return err
	}
	return d.burstWrite(regFifo, data)
}

func (d *Device) setPayloadLength(length byte) error {
	return d.singleWrite(regPayloadLength, length)
}
