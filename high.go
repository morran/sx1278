/*
Copyright [2017] [morran]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sx1278

// InitTransiver ...
func (d *Device) InitTransiver() error {
	d.Reset()
	err := d.setBaseMode(ModeLORA | ModeLowFrequency)
	if err != nil {
		return err
	}
	return nil
}

// Configure ...
func (d *Device) Configure() error {
	return nil
}

// TransmitPacket ...
func (d *Device) TransmitPacket() {

}

// ReceivePacket ...
func (d *Device) ReceivePacket() {

}

// LastPacketRSSI ...
func (d *Device) LastPacketRSSI() int {
	temp := byte(10)
	temp, _ = d.singleRead(regPktRssiValue)
	return (-164 + int(temp))
}

// DeviceVersion Возвращает информацию из регистра regVersion
func (d *Device) DeviceVersion() byte {
	id, _ := d.singleRead(regVersion)
	return id
}
