/*
Copyright [2017] [morran]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sx1278

import (
	"time"

	rpio "github.com/stianeikeland/go-rpio"
	"golang.org/x/exp/io/spi"
)

// Device ...
type Device struct {
	port           *spi.Device
	SpreadFactor   int
	Bandwidth      int
	Power          int
	Frequency      int
	Timeout        int
	CheckCRC       bool
	CodeRate       int
	SyncWord       byte
	PreambleLength []byte
}

// Open ...
func (d *Device) Open() error {
	var err error
	d.port, err = spi.Open(&spi.Devfs{
		Dev:      "/dev/spidev0.1",
		Mode:     spi.Mode3,
		MaxSpeed: 7800000,
		// MaxSpeed: 500000,
	})

	if err != nil {
		return err
	}
	err = rpio.Open()
	if err != nil {
		return err
	}
	DIOs = make(map[string]rpio.Pin)
	for i, pinNum := range pinNums {
		DIOs[i] = rpio.Pin(pinNum)
		if i != "RESET" {
			DIOs[i].Input()
		} else {
			DIOs[i].Output()
		}
	}
	return err
}

// Close ...
func (d *Device) Close() error {
	rpio.Close()
	return d.port.Close()
}

// Reset ...
func (d *Device) Reset() {
	DIOs["RESET"].Low()
	time.Sleep(10 * time.Millisecond)
	DIOs["RESET"].High()
	time.Sleep(100 * time.Millisecond)
}

// // Init ...
// func (d *Device) Init() {
// 	d.initLoRa()
// }

// // CRCValid ...
// func (d *Device) CRCValid() bool {
// 	state := DIOs["DIO3"].Read()
// 	return state != rpio.High
// }

// func (d *Device) getDIO0() bool {
// 	state := DIOs["DIO0"].Read()
// 	return state != rpio.High
// }

// func (d *Device) initLoRa() {
// 	d.Sleep()
// 	time.Sleep(20 * time.Millisecond)
// 	d.modeLoRa()
// 	d.burstWrite(regFrMsb, Frequency[d.Frequency])
// 	d.singleWrite(regPaConfig, Power[d.Power])
// 	d.singleWrite(regOcp, 0x0b)
// 	d.singleWrite(regLna, 0x23)
// 	if d.SpreadFactor == SpreadingFactor6 {
// 		d.singleWrite(regModemConfig1, (byte(d.Bandwidth<<4))+(byte(d.CodeRate<<1))+0x01)
// 		crc := byte(0x00)
// 		if d.CheckCRC {
// 			crc = byte(0x01)
// 		}
// 		d.singleWrite(regModemConfig2, (spreadFactor[d.SpreadFactor]<<4)+(crc<<2)+0x03)
// 		buf, _ := d.singleRead(regDetectOptimize)
// 		buf &= byte(0xf8)
// 		buf |= byte(0x05)
// 		d.singleWrite(regDetectOptimize, buf)
// 		d.singleWrite(regDetectionThreshold, 0x0c)
// 	} else {
// 		d.singleWrite(regModemConfig1, (byte(d.Bandwidth<<4))+(byte(d.CodeRate<<1))+byte(0x00))
// 		crc := byte(0x00)
// 		if d.CheckCRC {
// 			crc = byte(0x01)
// 		}
// 		d.singleWrite(regModemConfig2, (spreadFactor[d.SpreadFactor]<<4)+(crc<<2)+byte(0x03))
// 	}
// 	d.singleWrite(regSymbTimeoutLsb, 0xff)
// 	d.burstWrite(regPreambleMsb, d.PreambleLength)
// 	d.singleWrite(regSyncWord, d.SyncWord)
// 	d.Standby()
// }

// // EntryRX ...
// func (d *Device) EntryRX() bool {
// 	d.singleWrite(regPaDac, 0x84)
// 	d.singleWrite(regHopPeriod, 0xff)
// 	d.singleWrite(regDIOMapping1, 0x01)
// 	d.singleWrite(regDIOMapping2, 0x41)
// 	d.singleWrite(regIrqFlagsMask, 0x3f)
// 	d.ClearIRQ()
// 	d.singleWrite(regPayloadLength, maxPacketLength)
// 	d.singleWrite(regOpMode, 0x8d)
// 	timer := time.NewTimer(time.Duration(d.Timeout) * time.Millisecond).C
// 	for {
// 		recv, _ := d.singleRead(regModemStat)
// 		if (recv & byte(0x04)) == byte(0x04) {
// 			return true
// 		}
// 		select {
// 		case <-timer:
// 			return false
// 		default:
// 			continue
// 		}
// 	}
// }

// // ReceiveMessage ...
// func (d *Device) ReceiveMessage(length byte) []byte {
// 	var size byte
// 	var received []byte
// 	addr, _ := d.singleRead(regFifoRxBaseAddr)
// 	d.singleWrite(regFifoAddrPtr, addr)
// 	if d.getDIO0() {
// 		addr, _ = d.singleRead(regFifoRxCurrentAddr)
// 		d.singleWrite(regFifoAddrPtr, addr)
// 		if d.SpreadFactor == SpreadingFactor6 {
// 			size = length
// 		} else {
// 			size, _ = d.singleRead(regRxNbBytes)
// 		}
// 		received, _ = d.burstRead(regFifo, size)
// 	}
// 	return received
// }

// // EntryTX ...
// func (d *Device) EntryTX() {
// 	d.initLoRa()
// 	d.singleWrite(regPaDac, 0x84)
// 	d.singleWrite(regHopPeriod, 0x00)
// 	d.singleWrite(regDIOMapping1, 0x41)
// 	d.singleWrite(regDIOMapping2, 0x01)
// 	d.ClearIRQ()
// 	d.singleWrite(regIrqFlagsMask, 0xf7)
// }

// // TransmitMessage ...
// func (d *Device) TransmitMessage(data []byte) bool {
// 	fmt.Println(byte(len(data)))
// 	d.singleWrite(regPayloadLength, byte(len(data)))
// 	addr, _ := d.singleRead(regFifoTxBaseAddr)
// 	d.singleWrite(regFifoAddrPtr, addr)
// 	d.burstWrite(regFifo, data)
// 	d.singleWrite(regOpMode, 0x8b)
// 	timer := time.NewTimer(time.Duration(d.Timeout) * time.Millisecond).C
// 	for {
// 		if d.getDIO0() {
// 			return true
// 		}
// 		select {
// 		case <-timer:
// 			return false
// 		default:
// 			continue
// 		}
// 	}
// }
