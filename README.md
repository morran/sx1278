# SX1278 #

Semtech sx1278 library for Raspberry pi 2 written in Golang (NOT WORKING NOW!)

## Example usage (NOT WORKING) ##

```Golang
package main

import (
	"encoding/binary"
	"log"

	"git.morran.xyz/morran/sx1278"
)

func main() {
	preambleLength := make([]byte, 2)
	binary.LittleEndian.PutUint16(preambleLength, 320)
	device := sx1278.Device{
		Bandwidth:      sx1278.BandWidth250KHz,
		CodeRate:       sx1278.CodingRrate4to8,
		CheckCRC:       true,
		Frequency:      sx1278.Frequency434MHz,
		Power:          sx1278.Power17DBm,
		PreambleLength: preambleLength,
		SpreadFactor:   sx1278.SpreadingFactor12,
		SyncWord:       0x34,
	}
	err := device.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer device.Close()
	device.Init()
	device.EntryTX()
	device.TransmitMessage([]byte("Hello, world!"))
	if device.EntryRX() {
		for {
			data := device.ReceiveMessage(0xff) // param needed if SpreadingFactor == 6
			log.Printf("%+v", data)
		}
	}
}
```